//
//  SyncPersistentStorage.swift
//  FlickrClient
//
//  Created by Efimov, Michael on 13/04/2019.
//  Copyright © 2019 Efimov, Michael. All rights reserved.
//

import Foundation

protocol SyncPersistentStorageProtocol {
    func preload() -> Set<String>
    func save(id: String, data: Data) -> Bool
    func load(id: String) -> Data?
    func delete(id: String)
}

protocol FCFileManager {
    func urls(for directory: FileManager.SearchPathDirectory, in domainMask: FileManager.SearchPathDomainMask) -> [URL]
    func createDirectory(at url: URL, withIntermediateDirectories createIntermediates: Bool, attributes: [FileAttributeKey : Any]?) throws
    func createFile(atPath path: String, contents data: Data?, attributes attr: [FileAttributeKey : Any]?) -> Bool
    func contentsOfDirectory(atPath path: String) throws -> [String]
    func replaceItemAt(_ originalItemURL: URL, withItemAt newItemURL: URL, backupItemName: String?, options: FileManager.ItemReplacementOptions) throws -> URL?
    func contents(atPath path: String) -> Data?
    func removeItem(at URL: URL) throws
}

extension FileManager: FCFileManager {
}

class SyncPersistentStorage: SyncPersistentStorageProtocol {
    init(fileManager: FCFileManager = FileManager.default,
         basePath: String = "ImageCache") {
        self.fileManager = fileManager
        let baseURL = fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0]
        self.baseURL = baseURL.appendingPathComponent(basePath)
    }

    func preload() -> Set<String> {
        try? fileManager.createDirectory(at: baseURL, withIntermediateDirectories: true, attributes: nil)
        var ids = Set<String>()
        guard let items = try? fileManager.contentsOfDirectory(atPath: baseURL.path) else {
            return ids
        }
        for item in items {
            if item.hasSuffix(backupSuffix) {
                let url = baseURL.appendingPathComponent(item)
                if (try? fileManager.removeItem(at: url)) == nil {
                    assertionFailure()
                }
                continue
            }
            ids.insert(item)
        }
        return ids
    }

    func save(id: String, data: Data) -> Bool {
        let backupURL = buildBackupURL(for: id)
        let origURL = buildURL(for: id)
        if fileManager.createFile(atPath: backupURL.path, contents: data, attributes: nil) {
            if let res = try? fileManager.replaceItemAt(origURL, withItemAt: backupURL, backupItemName: nil, options: .usingNewMetadataOnly), res != nil {
                return true
            }
        }
        return false
    }

    func load(id: String) -> Data? {
        let url = buildURL(for: id)
        return fileManager.contents(atPath: url.path)
    }

    func delete(id: String) {
        let url = buildURL(for: id)
        try? fileManager.removeItem(at: url)
    }

    private func buildURL(for id: String) -> URL {
        return baseURL.appendingPathComponent(id)
    }

    private func buildBackupURL(for id: String) -> URL {
        return baseURL.appendingPathComponent(id + backupSuffix)
    }

    private let backupSuffix = ".back"
    private let fileManager: FCFileManager
    private let baseURL: URL
}
