//
//  AsyncPersistentStorage.swift
//  FlickrClient
//
//  Created by Efimov, Michael on 13/04/2019.
//  Copyright © 2019 Efimov, Michael. All rights reserved.
//

import Foundation

protocol AsyncPersistentStorageProtocol {
    func preload(callbackTaskRunner: TaskRunner,
                 callback: @escaping (Set<String>) -> Void)
    func save(id: String, data: Data)
    func load(id: String,
              callbackTaskRunner: TaskRunner,
              callback: @escaping (Data?) -> Void)
    func delete(id: String)
}

class AsyncPersistentStorage: AsyncPersistentStorageProtocol {
    init(syncStorage: SyncPersistentStorageProtocol = SyncPersistentStorage(),
         taskRunner: SequencedTaskRunner = GCDBackgroundQueueTaskRunner(name: "AsyncPersistentStorage")) {
        self.syncStorage = syncStorage
        self.taskRunner = taskRunner
    }

    func preload(callbackTaskRunner: TaskRunner,
                 callback: @escaping (Set<String>) -> Void) {
        withSyncStorage { storage in
            let ids = storage.preload()
            callbackTaskRunner.postTask {
                callback(ids)
            }
        }
    }

    func save(id: String, data: Data) {
        withSyncStorage { storage in
            _ = storage.save(id: id, data: data)
        }
    }

    func load(id: String,
              callbackTaskRunner: TaskRunner,
              callback: @escaping (Data?) -> Void) {
        withSyncStorage { storage in
            let data = storage.load(id: id)
            callbackTaskRunner.postTask {
                callback(data)
            }
        }
    }

    func delete(id: String) {
        withSyncStorage { storage in
            storage.delete(id: id)
        }
    }

    private func withSyncStorage(task: @escaping (SyncPersistentStorageProtocol) -> Void) {
        let storage = syncStorage
        taskRunner.postTask {
            task(storage)
        }
    }

    private let syncStorage: SyncPersistentStorageProtocol
    private let taskRunner: SequencedTaskRunner
}
