//
//  AsyncPersistentStorageTests.swift
//  FlickrClient
//
//  Created by Efimov, Michael on 14/04/2019.
//  Copyright © 2019 Efimov, Michael. All rights reserved.
//

import Foundation
import XCTest

@testable import FlickrClient

class AsyncPersistentStorageTests: XCTestCase {
    override func setUp() {
        syncStorage = FakeSyncStorage()
        taskRunner = TestTaskRunner()
        me = AsyncPersistentStorage(syncStorage: syncStorage, taskRunner: taskRunner)
    }

    override func tearDown() {
        me = nil
        syncStorage = nil
        taskRunner = nil
    }

    func testPreload() {
        let startupItems = Set<String>(["1", "2"])
        syncStorage.startupItems = startupItems
        var receivedStartupItems: Set<String>?
        me.preload(callbackTaskRunner: taskRunner) { items in
            receivedStartupItems = items
        }

        taskRunner.runTasksUntilIdle()

        XCTAssertEqual(startupItems, receivedStartupItems)
    }

    func testSave() {
        let data = "sdfsdf".data(using: .utf8)!
        let id = "1"
        me.save(id: id, data: data)

        XCTAssertNil(syncStorage.load(id: id))
        taskRunner.runTasksUntilIdle()
        XCTAssertEqual(syncStorage.load(id: id), data)
    }

    func testLoad() {
        let data = "sdfsdf".data(using: .utf8)!
        let id = "1"
        _ = syncStorage.save(id: id, data: data)
        var receivedData: Data?
        me.load(id: id, callbackTaskRunner: taskRunner) { indata in
            receivedData = indata
        }
        taskRunner.runTasksUntilIdle()

        XCTAssertEqual(receivedData, data)
    }

    func testDelete() {
        let data = "sdfsdf".data(using: .utf8)!
        let id = "1"
        _ = syncStorage.save(id: id, data: data)
        me.delete(id: id)
        taskRunner.runTasksUntilIdle()
        XCTAssertNil(syncStorage.load(id: id))
    }

    private var me: AsyncPersistentStorage!
    private var syncStorage: FakeSyncStorage!
    private var taskRunner: TestTaskRunner!
}
