//
//  FakeSyncStorage.swift
//  FlickrClient
//
//  Created by Efimov, Michael on 13/04/2019.
//  Copyright © 2019 Efimov, Michael. All rights reserved.
//

import Foundation

@testable import FlickrClient

class FakeSyncStorage: SyncPersistentStorageProtocol {

    var startupItems = Set<String>()

    func preload() -> Set<String> {
        return startupItems
    }

    func save(id: String, data: Data) -> Bool {
        items[id] = data
        return true
    }

    func load(id: String) -> Data? {
        return items[id]
    }

    func delete(id: String) {
        items.removeValue(forKey: id)
    }

    private var items = [String: Data]()
}
