//
//  URLLoader.swift
//  FlickrClient
//
//  Created by Efimov, Michael on 13/04/2019.
//  Copyright © 2019 Efimov, Michael. All rights reserved.
//

import Foundation

protocol URLLoaderProtocol: AnyObject {
    var url: URL { get }
    func resume()
}

class URLLoader: URLLoaderProtocol {
    init(url: URL,
         session: URLSession,
         disableCache: Bool,
         callbackTaskRunner: TaskRunner,
         callback: @escaping URLLoaderFactoryProtocol.LoadCallback) {
        self.url = url
        self.callbackTaskRunner = callbackTaskRunner
        self.callback = callback

        let request = URLRequest(url: url,
                                 cachePolicy: disableCache ? .reloadIgnoringLocalCacheData : .useProtocolCachePolicy,
                                 timeoutInterval: TimeInterval(30))
        task = session.dataTask(with: request) { [weak self] data, response, error in
            self?.onCompletion(url: url, data: data, response: response, error: error)
        }
    }

    let url: URL

    func resume() {
        task.resume()
    }

    private func onCompletion(url: URL, data: Data?, response: URLResponse?, error: Error?) {
        if data == nil {
            print("\(url)")
        }
        task = nil
        if let callback = self.callback {
            self.callback = nil
            callbackTaskRunner.postTask {
                callback(data)
            }
        }
    }

    private var callbackTaskRunner: TaskRunner
    private var callback: URLLoaderFactoryProtocol.LoadCallback?
    private var task: URLSessionDataTask!
}
