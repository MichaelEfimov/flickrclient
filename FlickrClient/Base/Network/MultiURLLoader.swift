//
//  MultiURLLoader.swift
//  FlickrClient
//
//  Created by Efimov, Michael on 13/04/2019.
//  Copyright © 2019 Efimov, Michael. All rights reserved.
//

import Foundation

protocol MultiURLLoader: AnyObject {
    typealias LoadCallback = (Data?) -> Void
    func addDownloadTask(url: URL, loadCallback: @escaping LoadCallback)
    func cancelPending()
    var callbackTaskRunner: SequencedTaskRunner? { get set }
}
