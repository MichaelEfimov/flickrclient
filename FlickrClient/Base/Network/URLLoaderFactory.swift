//
//  URLLoaderFactory.swift
//  FlickrClient
//
//  Created by Efimov, Michael on 13/04/2019.
//  Copyright © 2019 Efimov, Michael. All rights reserved.
//

import Foundation

protocol URLLoaderFactoryProtocol: AnyObject {
    typealias LoadCallback = (Data?) -> Void

    func buildURLLoader(url: URL,
                        disableCache: Bool,
                        callbackTaskRunner: TaskRunner,
                        callback: @escaping LoadCallback) -> URLLoaderProtocol
}

class URLLoaderFactory: URLLoaderFactoryProtocol {

    init(session: URLSession = URLSession(configuration: URLSessionConfiguration.default)) {
        self.session = session
    }

    func buildURLLoader(url: URL,
                        disableCache: Bool,
                        callbackTaskRunner: TaskRunner,
                        callback: @escaping LoadCallback) -> URLLoaderProtocol {
        return URLLoader(url: url,
                         session: session,
                         disableCache: disableCache,
                         callbackTaskRunner: callbackTaskRunner,
                         callback: callback)
    }

    private let session: URLSession
}
