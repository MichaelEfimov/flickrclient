//
//  ParallelURLLoader.swift
//  FlickrClient
//
//  Created by Efimov, Michael on 13/04/2019.
//  Copyright © 2019 Efimov, Michael. All rights reserved.
//

import Foundation
import UIKit

class ParallelURLLoader: MultiURLLoader {
    init(maxParallel: Int = 16,
         disableCache: Bool = false,
         urlLoaderFactory: URLLoaderFactoryProtocol = URLLoaderFactory(),
         networkTaskRunner: SequencedTaskRunner = GCDBackgroundQueueTaskRunner(name: "ParallelURLLoaderNetwork")) {
        assert(maxParallel > 0)
        self.maxParallel = maxParallel
        self.disableCache = disableCache
        self.urlLoaderFactory = urlLoaderFactory
        self.networkTaskRunner = networkTaskRunner
    }

    var callbackTaskRunner: SequencedTaskRunner?

    func addDownloadTask(url: URL, loadCallback: @escaping LoadCallback) {
        networkTaskRunner.postTask { [weak self] in
            guard let self = self else {
                return
            }
            self.queue.pushBack((url, loadCallback))
            self.runPendingTasks()
        }
    }

    func cancelPending() {
        networkTaskRunner.postTask { [weak self] in
            self?.queue = LinkedList<(URL, LoadCallback)>()
        }
    }

    private func runPendingTasks() {
        while !queue.empty && loaders.count < maxParallel {
            let (url, loadCallback) = queue.front
            queue.popFront()
            if loaders[url] != nil {
                loaders[url]!.callbacks.append(loadCallback)
                continue
            }
            let urlLoader = urlLoaderFactory.buildURLLoader(url: url,
                                                            disableCache: disableCache,
                                                            callbackTaskRunner: networkTaskRunner) { [weak self] data in
                self?.onURLLoaded(url: url, data: data)
            }
            loaders[url] = LoadData(loader: urlLoader, callbacks: [loadCallback])
            urlLoader.resume()
        }
    }

    private func onURLLoaded(url: URL, data: Data?) {
        let callbacks = loaders[url]!.callbacks
        loaders.removeValue(forKey: url)
        let task = {
            for callback in callbacks {
                callback(data)
            }
        }
        if let callbackTaskRunner = self.callbackTaskRunner {
            callbackTaskRunner.postTask(task)
        } else {
            task()
        }
        runPendingTasks()
    }

    private struct LoadData {
        let loader: URLLoaderProtocol
        var callbacks: [LoadCallback]
    }

    private let maxParallel: Int
    private let disableCache: Bool
    private let urlLoaderFactory: URLLoaderFactoryProtocol
    private let networkTaskRunner: SequencedTaskRunner
    private var queue = LinkedList<(URL, LoadCallback)>()
    private var loaders = [URL: LoadData]()
}
