//
//  ParallelURLLoaderTests.swift
//  FlickrClient
//
//  Created by Efimov, Michael on 13/04/2019.
//  Copyright © 2019 Efimov, Michael. All rights reserved.
//

import Foundation
import XCTest

@testable import FlickrClient

class ParallelURLLoaderTests: XCTestCase {
    override func setUp() {
        urlLoaderFactory = FakeURLLoaderFactory()
        taskRunner = TestTaskRunner()
    }

    override func tearDown() {
        urlLoaderFactory = nil
        taskRunner = nil
        me = nil
    }

    func testSingleLoad() {
        buildMyself()
        let data = buildData(index: 0)
        let url = buildURL(index: 0)
        urlLoaderFactory.bindResponse(url: url, response: data)
        var callbackCalled = false
        me.addDownloadTask(url: url) { receivedData in
            XCTAssertEqual(data, receivedData)
            callbackCalled = true
        }
        XCTAssertFalse(callbackCalled)
        taskRunner.runTasksUntilIdle()
        XCTAssertTrue(callbackCalled)
    }

    func testParallelLoad() {
        buildMyself(maxParallel: 2)
        var sentURLs = URLDataSet()
        var receivedURLs = URLDataSet()
        for index in 0 ..< 2 {
            let data = buildData(index: index)
            let url = buildURL(index: index)
            sentURLs.insert(URLData(url: url, data: data))
            urlLoaderFactory.bindResponse(url: url, response: data)
            me.addDownloadTask(url: url) { receivedData in
                receivedURLs.insert(URLData(url: url, data: receivedData))
            }
        }

        XCTAssertEqual(urlLoaderFactory.numberOfBlockedRequests, 0)
        urlLoaderFactory.blockRequests = true
        taskRunner.runTasksUntilIdle()

        XCTAssertEqual(receivedURLs, URLDataSet())
        XCTAssertEqual(urlLoaderFactory.numberOfBlockedRequests, 2)

        urlLoaderFactory.blockRequests = false
        taskRunner.runTasksUntilIdle()
        XCTAssertEqual(sentURLs, receivedURLs)
    }

    func testQueueingLoad() {
        buildMyself(maxParallel: 1)
        var sentURLs = URLDataSet()
        var receivedURLs = URLDataSet()
        for index in 0 ..< 2 {
            let data = buildData(index: index)
            let url = buildURL(index: index)
            sentURLs.insert(URLData(url: url, data: data))
            urlLoaderFactory.bindResponse(url: url, response: data)
            me.addDownloadTask(url: url) { receivedData in
                receivedURLs.insert(URLData(url: url, data: receivedData))
            }
        }

        XCTAssertEqual(urlLoaderFactory.numberOfBlockedRequests, 0)
        urlLoaderFactory.blockRequests = true
        taskRunner.runTasksUntilIdle()

        XCTAssertEqual(receivedURLs, URLDataSet())
        XCTAssertEqual(urlLoaderFactory.numberOfBlockedRequests, 1)

        urlLoaderFactory.blockRequests = false
        taskRunner.runTasksUntilIdle()
        XCTAssertEqual(sentURLs, receivedURLs)
    }

    private struct URLData: Hashable {
        let url: URL
        let data: Data?
    }

    private typealias URLDataSet = Set<URLData>

    private func buildURL(index: Int) -> URL {
        return URL(string: "http://\(index).com/")!
    }

    private func buildData(index: Int) -> Data {
        return "\(index)".data(using: .utf8)!
    }

    private func buildMyself(maxParallel: Int = 1) {
        me = ParallelURLLoader(maxParallel: maxParallel, urlLoaderFactory: urlLoaderFactory, networkTaskRunner: taskRunner)
    }

    private var me: ParallelURLLoader!
    private var urlLoaderFactory: FakeURLLoaderFactory!
    private var taskRunner: TestTaskRunner!
}
