//
//  FakeMultiURLLoader.swift
//  FlickrClientTests
//
//  Created by Efimov, Michael on 13/04/2019.
//  Copyright © 2019 Efimov, Michael. All rights reserved.
//

import Foundation

@testable import FlickrClient

class FakeMultiURLLoader: MultiURLLoader {

    func bindResponse(url: URL, data: Data) {
        responses[url] = data
    }

    func removeAllBinds() {
        responses.removeAll()
    }

    func addDownloadTask(url: URL, loadCallback: @escaping LoadCallback) {
        loadCallback(responses[url])
    }

    func cancelPending() {
    }

    var callbackTaskRunner: SequencedTaskRunner?

    private var responses = [URL: Data]()
}
