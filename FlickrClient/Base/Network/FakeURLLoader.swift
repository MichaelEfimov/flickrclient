//
//  FakeURLLoader.swift
//  FlickrClientTests
//
//  Created by Efimov, Michael on 13/04/2019.
//  Copyright © 2019 Efimov, Michael. All rights reserved.
//

import Foundation

@testable import FlickrClient

class FakeURLLoader: URLLoaderProtocol {
    init(url: URL,
         callbackTaskRunner: TaskRunner,
         callback: @escaping URLLoaderFactoryProtocol.LoadCallback,
         response: Data?,
         throttler: @escaping (@escaping ()->Void) -> Void) {
        self.url = url
        self.callbackTaskRunner = callbackTaskRunner
        self.callback = callback
        self.response = response
        self.throttler = throttler
    }
    let url: URL

    func resume() {
        let response = self.response
        let callback = self.callback
        let callbackTaskRunner = self.callbackTaskRunner
        throttler({
            callbackTaskRunner.postTask {
                callback(response)
            }
        })
    }

    let callbackTaskRunner: TaskRunner
    let callback: URLLoaderFactoryProtocol.LoadCallback
    let response: Data?
    let throttler: (@escaping ()->Void) -> Void
}

class FakeURLLoaderFactory: URLLoaderFactoryProtocol {
    func bindResponse(url: URL, response: Data) {
        bindMap[url] = response
    }

    var blockRequests = false {
        didSet {
            if !blockRequests {
                let blockedRequests = self.blockedRequests
                self.blockedRequests = []
                for block in blockedRequests {
                    block()
                }
            }
        }
    }

    var numberOfBlockedRequests: Int {
        return blockedRequests.count
    }

    func buildURLLoader(url: URL,
                        disableCache: Bool,
                        callbackTaskRunner: TaskRunner,
                        callback: @escaping LoadCallback) -> URLLoaderProtocol {
        return FakeURLLoader(url: url,
                             callbackTaskRunner: callbackTaskRunner,
                             callback: callback,
                             response: bindMap[url],
                             throttler: { block in
                                if self.blockRequests {
                                    self.blockedRequests.append(block)
                                } else {
                                    block()
                                }
        })
    }

    private var bindMap = [URL: Data]()
    private var blockedRequests = [()->Void]()
}
