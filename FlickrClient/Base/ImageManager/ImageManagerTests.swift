//
//  ImageManagerTests.swift
//  FlickrClient
//
//  Created by Efimov, Michael on 14/04/2019.
//  Copyright © 2019 Efimov, Michael. All rights reserved.
//

import Foundation
import XCTest

@testable import FlickrClient

class ImageManagerTests: XCTestCase {
    override func setUp() {
        storage = FakeSyncStorage()
        urlLoader = FakeMultiURLLoader()
        taskRunner = TestTaskRunner()
        imageConverter = FakeImageConverter()
        memoryWarningObserver = FakeMemoryWarningObserver()
        me = ImageManager(storage: AsyncPersistentStorage(syncStorage: storage, taskRunner: taskRunner),
                          convertTaskRunner: taskRunner,
                          mainThreadTaskRunner: taskRunner,
                          imageConverter: imageConverter,
                          urlLoader: urlLoader,
                          memoryWarningObserver: memoryWarningObserver)
    }

    override func tearDown() {
        me = nil
        storage = nil
        urlLoader = nil
        taskRunner = nil
        memoryWarningObserver = nil
    }

    func testSingleLoadBeforePreload() {
        let id = "0"
        let url = buildURL(index: 0)
        let data = buildData(index: 0)
        let image = TestImages.images[0]
        urlLoader.bindResponse(url: url, data: data)
        imageConverter.bind(data: data, image: image)
        var receivedImage: UIImage?
        me.loadImage(id: id, url: url) { image in
            receivedImage = image
        }
        taskRunner.runTasksUntilIdle()
        XCTAssertEqual(receivedImage, image)
        XCTAssertEqual(storage.load(id: id), data)
    }

    func testSingleLoadAfterPreload() {
        taskRunner.runTasksUntilIdle()
        let id = "0"
        let url = buildURL(index: 0)
        let data = buildData(index: 0)
        let image = TestImages.images[0]
        urlLoader.bindResponse(url: url, data: data)
        imageConverter.bind(data: data, image: image)
        var receivedImage: UIImage?
        me.loadImage(id: id, url: url) { image in
            receivedImage = image
        }
        taskRunner.runTasksUntilIdle()
        XCTAssertEqual(receivedImage, image)
        XCTAssertEqual(storage.load(id: id), data)
    }

    func testDoubleLoad() {
        let id = "0"
        let url = buildURL(index: 0)
        let data = buildData(index: 0)
        let image = TestImages.images[0]
        urlLoader.bindResponse(url: url, data: data)
        imageConverter.bind(data: data, image: image)
        var receivedImage1: UIImage?
        me.loadImage(id: id, url: url) { image in
            receivedImage1 = image
        }

        var receivedImage2: UIImage?
        me.loadImage(id: id, url: url) { image in
            receivedImage2 = image
        }

        taskRunner.runTasksUntilIdle()
        XCTAssertEqual(receivedImage1, image)
        XCTAssertEqual(receivedImage2, image)
        XCTAssertEqual(storage.load(id: id), data)
    }

    func testLoadTwoItems() {
        var expectedImages = [UIImage?]()
        var receivedImages = [UIImage?]()
        for index in 0 ..< 2 {
            let id = "\(index)"
            let url = buildURL(index: index)
            let data = buildData(index: index)
            let image = TestImages.images[index]
            expectedImages.append(image)
            urlLoader.bindResponse(url: url, data: data)
            imageConverter.bind(data: data, image: image)
            me.loadImage(id: id, url: url) { image in
                receivedImages.append(image)
            }
        }
        taskRunner.runTasksUntilIdle()
        XCTAssertEqual(receivedImages, expectedImages)
    }

    func testFailedRetry() {
        taskRunner.runTasksUntilIdle()
        let id = "0"
        let url = buildURL(index: 0)
        let data = buildData(index: 0)
        let image = TestImages.images[0]
        imageConverter.bind(data: data, image: image)

        var receivedImage: UIImage?
        me.loadImage(id: id, url: url) { image in
            receivedImage = image
        }
        taskRunner.postTask {
            self.urlLoader.bindResponse(url: url, data: data)
        }
        taskRunner.runTasksUntilIdle()
        XCTAssertEqual(receivedImage, image)
    }

    func testLoadFromMemoryCache() {
        let id = "0"
        let url = buildURL(index: 0)
        let data = buildData(index: 0)
        let image = TestImages.images[0]
        urlLoader.bindResponse(url: url, data: data)
        imageConverter.bind(data: data, image: image)
        var receivedImage: UIImage?
        me.loadImage(id: id, url: url) { image in
            receivedImage = image
        }
        taskRunner.runTasksUntilIdle()
        XCTAssertEqual(receivedImage, image)

        receivedImage = nil
        urlLoader.removeAllBinds()
        storage.delete(id: id)
        me.loadImage(id: id, url: url) { image in
            receivedImage = image
        }
        XCTAssertEqual(receivedImage, image)
    }

    func testLoadFromStorage() {
        let id = "0"
        let url = buildURL(index: 0)
        let data = buildData(index: 0)
        let image = TestImages.images[0]
        storage.startupItems.insert(id)
        _ = storage.save(id: id, data: data)
        imageConverter.bind(data: data, image: image)
        var receivedImage: UIImage?
        me.loadImage(id: id, url: url) { image in
            receivedImage = image
        }
        taskRunner.runTasksUntilIdle()
        XCTAssertEqual(receivedImage, image)
    }

    func testLoadFromStorageOnMemoryWarning() {
        let id = "0"
        let url = buildURL(index: 0)
        let data = buildData(index: 0)
        let newData = buildData(index: 1)
        let image = TestImages.images[0]
        let newImage = TestImages.images[0]
        urlLoader.bindResponse(url: url, data: data)
        imageConverter.bind(data: data, image: image)
        imageConverter.bind(data: newData, image: newImage)

        var receivedImage: UIImage?
        me.loadImage(id: id, url: url) { image in
            receivedImage = image
        }
        taskRunner.runTasksUntilIdle()

        XCTAssertEqual(receivedImage, image)

        memoryWarningObserver.simulateMemoryWarning()

        _ = storage.save(id: id, data: newData)

        receivedImage = nil
        me.loadImage(id: id, url: url) { image in
            receivedImage = image
        }
        taskRunner.runTasksUntilIdle()

        XCTAssertEqual(receivedImage, newImage)
    }

    func testCancelBeforePreload() {
        let id = "0"
        let url = buildURL(index: 0)
        let data = buildData(index: 0)
        let image = TestImages.images[0]
        urlLoader.bindResponse(url: url, data: data)
        imageConverter.bind(data: data, image: image)
        var callbackReceived = false
        me.loadImage(id: id, url: url) { image in
            callbackReceived = true
        }
        me.cancelPending()
        taskRunner.runTasksUntilIdle()
        XCTAssertFalse(callbackReceived)
    }

    func testCancelAfterPreload() {
        let id = "0"
        let url = buildURL(index: 0)
        let data = buildData(index: 0)
        let image = TestImages.images[0]
        urlLoader.bindResponse(url: url, data: data)
        imageConverter.bind(data: data, image: image)
        var callbackReceived = false
        me.loadImage(id: id, url: url) { image in
            callbackReceived = true
        }
        me.cancelPending()
        taskRunner.runTasksUntilIdle()
        XCTAssertFalse(callbackReceived)
    }

    private func buildURL(index: Int) -> URL {
        return URL(string: "http://\(index).com/")!
    }

    private func buildData(index: Int) -> Data {
        return "\(index)".data(using: .utf8)!
    }

    private var me: ImageManager!
    private var storage: FakeSyncStorage!
    private var taskRunner: TestTaskRunner!
    private var urlLoader: FakeMultiURLLoader!
    private var imageConverter: FakeImageConverter!
    private var memoryWarningObserver: FakeMemoryWarningObserver!
}

private class FakeMemoryWarningObserver: MemoryWarningObserverProtocol {
    weak var delegate: MemoryWarningObserverDelegate?

    func simulateMemoryWarning() {
        delegate?.didReceiveMemoryWarning()
    }
}
