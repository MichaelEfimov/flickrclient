//
//  MemoryWarningObserver.swift
//  FlickrClient
//
//  Created by Efimov, Michael on 14/04/2019.
//  Copyright © 2019 Efimov, Michael. All rights reserved.
//

import Foundation
import UIKit

protocol MemoryWarningObserverDelegate: AnyObject {
    func didReceiveMemoryWarning()
}

protocol MemoryWarningObserverProtocol: AnyObject {
    var delegate: MemoryWarningObserverDelegate? { get set }
}

class MemoryWarningObserver: MemoryWarningObserverProtocol {
    init(notificationCenter: Foundation.NotificationCenter = Foundation.NotificationCenter.default) {
        self.notificationCenter = notificationCenter
        self.observer = self.subscribeToMemoryWarnings(notificationCenter: notificationCenter)
    }

    deinit {
        if let observer = self.observer {
            self.notificationCenter.removeObserver(observer)
        }
    }

    weak var delegate: MemoryWarningObserverDelegate?

    private func subscribeToMemoryWarnings(notificationCenter: Foundation.NotificationCenter) -> NSObjectProtocol {
        return notificationCenter.addObserver(forName: UIApplication.didReceiveMemoryWarningNotification,
                                              object: nil,
                                              queue: nil,
                                              using: { [weak self] _ in
                                                self?.delegate?.didReceiveMemoryWarning()
        })
    }

    private let notificationCenter: Foundation.NotificationCenter
    private var observer: NSObjectProtocol?
}
