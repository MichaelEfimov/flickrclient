//
//  TestImages.swift
//  FlickrClientTests
//
//  Created by Efimov, Michael on 14/04/2019.
//  Copyright © 2019 Efimov, Michael. All rights reserved.
//

import Foundation
import UIKit

class TestImages {
    static let images = [
        loadImage(named: "flickr.ico"),
        loadImage(named: "uber.ico"),
        loadImage(named: "github.ico")
    ]

    static func loadImage(named: String) -> UIImage {
        let bundle = Bundle(for: TestImages.self)
        return UIImage(named: named, in: bundle, compatibleWith: nil)!
    }
}
