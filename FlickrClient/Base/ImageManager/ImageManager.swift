//
//  ImageManager.swift
//  FlickrClient
//
//  Created by Efimov, Michael on 13/04/2019.
//  Copyright © 2019 Efimov, Michael. All rights reserved.
//

import Foundation
import UIKit

protocol ImageManagerProtocol: AnyObject {
    typealias LoadCalllback = (UIImage?) -> Void
    func loadImage(id: String, url: URL, callback: @escaping LoadCalllback)
    func cancelPending()
}

class ImageManager: ImageManagerProtocol {

    init(storage: AsyncPersistentStorageProtocol = AsyncPersistentStorage(),
         convertTaskRunner: SequencedTaskRunner = GCDBackgroundQueueTaskRunner(name: "ImageManagerConvert"),
         mainThreadTaskRunner: MainThreadTaskRunner = GCDQueueMainThreadTaskRunner(),
         imageConverter: ImageConverterProtocol = ImageConverter(),
         urlLoader: MultiURLLoader = ParallelURLLoader(disableCache: true),
         memoryWarningObserver: MemoryWarningObserverProtocol = MemoryWarningObserver()) {
        self.storage = storage
        self.convertTaskRunner = convertTaskRunner
        self.mainThreadTaskRunner = mainThreadTaskRunner
        self.urlLoader = urlLoader
        self.urlLoader.callbackTaskRunner = mainThreadTaskRunner
        self.imageConverter = imageConverter
        self.memoryWarningObserver = memoryWarningObserver

        memoryWarningObserver.delegate = self

        storage.preload(callbackTaskRunner: mainThreadTaskRunner) { [weak self] ids in
            self?.startup(storedIDs: ids)
        }
    }

    func loadImage(id: String, url: URL, callback: @escaping LoadCalllback) {
        guard persistentCache != nil else {
            preStartupQueue.append((id, url, callback))
            return
        }
        doLoadImage(id: id, url: url, callback: callback)
    }

    func cancelPending() {
        self.urlLoader.cancelPending()
        self.preStartupQueue.removeAll()
        processedImages.removeAll()
        self.version = Version()
    }

    private func startup(storedIDs: Set<String>) {
        persistentCache = LRUCache<String, Int>(maxSize: 5000)
        persistentCache.evictCallback = { [weak self] key, value in
            self?.storage.delete(id: key)
        }

        for item in storedIDs {
            persistentCache.put(key: item, value: 0)
        }

        let preStartupQueue = self.preStartupQueue
        self.preStartupQueue = []
        for (id, url, callback) in preStartupQueue {
            doLoadImage(id: id, url: url, callback: callback)
        }
    }

    private func doLoadImage(id: String, url: URL, callback: @escaping LoadCalllback) {
        if let memoryCachedImage = memoryCache.get(key: id) {
            _ = persistentCache.get(key: id)
            callback(memoryCachedImage)
            return
        }
        if processedImages[id] != nil {
            processedImages[id]!.append(callback)
            return
        }
        processedImages[id] = [callback]
        let version = self.version
        if let _ = persistentCache.get(key: id) {
            storage.load(id: id, callbackTaskRunner: mainThreadTaskRunner) { [weak self] data in
                self?.applyDataLoadedFromStorage(id: id, url: url, data: data, version: version)
            }
        } else {
            loadImageFromNetwork(id: id, url: url, version: version)
        }
    }

    private func applyDataLoadedFromStorage(id: String, url: URL, data: Data?, version: Version) {
        guard self.version === version else {
            return
        }
        if let data = data {
            convertImageData(id: id, url: url, data: data, version: version)
        } else {
            loadImageFromNetwork(id: id, url: url, version: version)
        }
    }

    private func loadImageFromNetwork(id: String, url: URL, version: Version) {
        urlLoader.addDownloadTask(url: url) { [weak self] data in
            self?.finalizeDownload(id: id, url: url, data: data, version: version)
        }
    }

    private func finalizeDownload(id: String, url: URL, data: Data?, version: Version) {
        guard self.version === version else {
            return
        }
        guard let data = data else {
            scheduleRetry(id: id, url: url, version: version)
            return
        }
        storage.save(id: id, data: data)
        persistentCache.put(key: id, value: 0)
        convertImageData(id: id, url: url, data: data, version: version)
    }

    private func scheduleRetry(id: String, url: URL, version: Version) {
        mainThreadTaskRunner.postDelayedTask(after: 1) { [weak self] in
            guard self?.version === version else {
                return
            }
            self?.loadImageFromNetwork(id: id, url: url, version: version)
        }
    }

    private func convertImageData(id: String, url: URL, data: Data, version: Version) {
        let mainThreadTaskRunner = self.mainThreadTaskRunner
        let imageConverter = self.imageConverter
        convertTaskRunner.postTask {
            let image = imageConverter.convert(data: data)
            mainThreadTaskRunner.postTask { [weak self] in
                self?.processConvertedImage(id: id, url: url, image: image, version: version)
            }
        }
    }

    private func processConvertedImage(id: String, url: URL, image: UIImage?, version: Version) {
        guard self.version === version else {
            return
        }
        if let image = image {
            memoryCache.put(key: id, value: image)
        }
        finalizeImage(id: id, image: image)
    }

    private func finalizeImage(id: String, image: UIImage?) {
        let callbacks = processedImages[id]
        processedImages.removeValue(forKey: id)
        assert(callbacks != nil)
        for callback in callbacks ?? [] {
            callback(image)
        }
    }

    private class Version {}

    private let storage: AsyncPersistentStorageProtocol
    private let convertTaskRunner: SequencedTaskRunner
    private let urlLoader: MultiURLLoader
    private let mainThreadTaskRunner: MainThreadTaskRunner
    private let imageConverter: ImageConverterProtocol
    private let memoryWarningObserver: MemoryWarningObserverProtocol
    private var preStartupQueue = [(String, URL, LoadCalllback)]()
    private var persistentCache: LRUCache<String, Int>!
    private var memoryCache = LRUCache<String, UIImage>(maxSize: 1000)
    private var processedImages = [String: [LoadCalllback]]()
    private var version = Version()
}

extension ImageManager: MemoryWarningObserverDelegate {
    func didReceiveMemoryWarning() {
        memoryCache.clear()
    }
}
