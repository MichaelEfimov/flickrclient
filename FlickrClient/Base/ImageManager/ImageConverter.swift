//
//  ImageConverter.swift
//  FlickrClient
//
//  Created by Efimov, Michael on 14/04/2019.
//  Copyright © 2019 Efimov, Michael. All rights reserved.
//

import Foundation
import UIKit

protocol ImageConverterProtocol: AnyObject {
    func convert(data: Data) -> UIImage?
}

class ImageConverter: ImageConverterProtocol {
    func convert(data: Data) -> UIImage? {
        return UIImage(data: data)
    }
}
