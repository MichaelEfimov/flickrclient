//
//  FakeImageConverter.swift
//  FlickrClientTests
//
//  Created by Efimov, Michael on 14/04/2019.
//  Copyright © 2019 Efimov, Michael. All rights reserved.
//

import Foundation
import UIKit

@testable import FlickrClient

class FakeImageConverter: ImageConverterProtocol {
    func bind(data: Data, image: UIImage) {
        bindMap[data] = image
    }

    func convert(data: Data) -> UIImage? {
        return bindMap[data]
    }

    private var bindMap = [Data: UIImage]()
}
