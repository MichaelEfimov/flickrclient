//
//  LRUCacheTests.swift
//  FlickrClientTests
//
//  Created by Efimov, Michael on 13/04/2019.
//  Copyright © 2019 Efimov, Michael. All rights reserved.
//

import Foundation
import XCTest

@testable import FlickrClient

class LRUCacheTests: XCTestCase {
    func testSizeOfOne() {
        var cache = LRUCache<Int, Int>(maxSize: 1)
        XCTAssertNil(cache.getWithoutPriority(key: 0))
        cache.put(key: 1, value: 10)
        XCTAssertNil(cache.getWithoutPriority(key: 0))
        XCTAssertEqual(cache.getWithoutPriority(key: 1), 10)

        cache.put(key: 1, value: 100)
        XCTAssertNil(cache.getWithoutPriority(key: 0))
        XCTAssertEqual(cache.getWithoutPriority(key: 1), 100)

        cache.put(key: 2, value: 20)
        XCTAssertNil(cache.getWithoutPriority(key: 0))
        XCTAssertNil(cache.getWithoutPriority(key: 1))
        XCTAssertEqual(cache.getWithoutPriority(key: 2), 20)
    }

    func testPutElimination() {
        var cache = LRUCache<Int, Int>(maxSize: 2)
        XCTAssertNil(cache.getWithoutPriority(key: 0))
        cache.put(key: 1, value: 10)
        XCTAssertNil(cache.getWithoutPriority(key: 0))
        XCTAssertEqual(cache.getWithoutPriority(key: 1), 10)

        cache.put(key: 1, value: 100)
        XCTAssertNil(cache.getWithoutPriority(key: 0))
        XCTAssertEqual(cache.getWithoutPriority(key: 1), 100)

        cache.put(key: 2, value: 20)
        XCTAssertNil(cache.getWithoutPriority(key: 0))
        XCTAssertEqual(cache.getWithoutPriority(key: 1), 100)
        XCTAssertEqual(cache.getWithoutPriority(key: 2), 20)

        cache.put(key: 1, value: 1000)
        XCTAssertEqual(cache.getWithoutPriority(key: 2), 20)
        XCTAssertEqual(cache.getWithoutPriority(key: 1), 1000)

        cache.put(key: 3, value: 30)
        XCTAssertNil(cache.getWithoutPriority(key: 2))
        XCTAssertEqual(cache.getWithoutPriority(key: 1), 1000)
        XCTAssertEqual(cache.getWithoutPriority(key: 3), 30)
    }

    func testGetElimination() {
        var cache = LRUCache<Int, Int>(maxSize: 2)
        cache.put(key: 1, value: 10)
        XCTAssertEqual(cache.getWithoutPriority(key: 1), 10)

        cache.put(key: 2, value: 20)
        XCTAssertEqual(cache.getWithoutPriority(key: 1), 10)
        XCTAssertEqual(cache.getWithoutPriority(key: 2), 20)

        _ = cache.get(key: 1)
        cache.put(key: 3, value: 30)

        XCTAssertNil(cache.getWithoutPriority(key: 2))
        XCTAssertEqual(cache.getWithoutPriority(key: 1), 10)
        XCTAssertEqual(cache.getWithoutPriority(key: 3), 30)
    }

    func testSizeOf5() {
        var cache = LRUCache<Int, Int>(maxSize: 5)
        cache.put(key: 1, value: 10)
        cache.put(key: 2, value: 20)
        cache.put(key: 3, value: 30)
        cache.put(key: 4, value: 40)
        cache.put(key: 5, value: 50)

        XCTAssertEqual(cache.getWithoutPriority(key: 1), 10)
        XCTAssertEqual(cache.getWithoutPriority(key: 2), 20)
        XCTAssertEqual(cache.getWithoutPriority(key: 3), 30)
        XCTAssertEqual(cache.getWithoutPriority(key: 4), 40)
        XCTAssertEqual(cache.getWithoutPriority(key: 5), 50)

        _ = cache.get(key: 1)
        _ = cache.get(key: 2)
        _ = cache.get(key: 4)
        _ = cache.get(key: 5)

        cache.put(key: 6, value: 60)
        XCTAssertNil(cache.getWithoutPriority(key: 3))
        XCTAssertEqual(cache.getWithoutPriority(key: 6), 60)
        XCTAssertEqual(cache.getWithoutPriority(key: 2), 20)
        XCTAssertEqual(cache.getWithoutPriority(key: 1), 10)
        XCTAssertEqual(cache.getWithoutPriority(key: 4), 40)
        XCTAssertEqual(cache.getWithoutPriority(key: 5), 50)

        cache.put(key: 7, value: 70)
        XCTAssertNil(cache.getWithoutPriority(key: 1))
        XCTAssertEqual(cache.getWithoutPriority(key: 6), 60)
        XCTAssertEqual(cache.getWithoutPriority(key: 2), 20)
        XCTAssertEqual(cache.getWithoutPriority(key: 4), 40)
        XCTAssertEqual(cache.getWithoutPriority(key: 5), 50)
        XCTAssertEqual(cache.getWithoutPriority(key: 7), 70)
    }
}

