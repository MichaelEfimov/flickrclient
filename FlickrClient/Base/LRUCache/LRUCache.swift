//
//  LRUCache.swift
//  FlickrClient
//
//  Created by Efimov, Michael on 13/04/2019.
//  Copyright © 2019 Efimov, Michael. All rights reserved.
//

import Foundation

struct LRUCache<Key : Hashable, Value> {
    typealias EvictCallback = (Key, Value) -> Void

    init(maxSize: Int) {
        assert(maxSize > 0)
        self.maxSize = maxSize
    }

    var evictCallback: EvictCallback?

    mutating func put(key: Key, value: Value) {
        let node: LLNode<NodeData> = {
            if let node = map[key] {
                list.popNode(node: node)
                node.v.value = value
                return node
            }
            if map.count == maxSize {
                if !list.empty {
                    let back = list.back
                    list.popBack()
                    map.removeValue(forKey: back.key)
                    if let evictCallback = evictCallback {
                        evictCallback(back.key, back.value)
                    }
                }
            }
            let node = LLNode(v: NodeData(key: key, value: value))
            map[key] = node
            return node
        }()
        list.pushFront(node: node)
    }

    mutating func get(key: Key) -> Value? {
        guard let node = map[key] else {
            return nil
        }
        list.popNode(node: node)
        list.pushFront(node: node)
        return node.v.value
    }

    func getWithoutPriority(key: Key) -> Value? {
        return map[key]?.v.value
    }

    mutating func clear() {
        map = [:]
        list = LinkedList<NodeData>()
    }

    private struct NodeData {
        let key: Key
        var value: Value
    }

    private let maxSize: Int
    private var map = [Key: LLNode<NodeData>]()
    private var list = LinkedList<NodeData>()
}
