//
//  LinkedList.swift
//  FlickrClient
//
//  Created by Efimov, Michael on 13/04/2019.
//  Copyright © 2019 Efimov, Michael. All rights reserved.
//

import Foundation

class LLNode<T> {
    init(v: T) {
        self.v = v
    }

    var v: T
    var next: LLNode?
    weak var prev: LLNode?
}

struct LinkedList<T> {

    mutating func pushFront(_ v: T) {
        let node = LLNode(v: v)
        pushFront(node: node)
    }

    mutating func pushBack(_ v: T) {
        let node = LLNode(v: v)
        pushBack(node: node)
    }

    mutating func pushFront(node: LLNode<T>) {
        insertBetween(left: nil, right: head, node: node)
    }

    mutating func pushBack(node: LLNode<T>) {
        insertBetween(left: tail, right: nil, node: node)
    }

    mutating func popFront() {
        popNode(node: head!)
    }

    mutating func popBack() {
        popNode(node: tail!)
    }

    mutating func popNode(node: LLNode<T>) {
        let left = node.prev
        let right = node.next
        left?.next = right
        right?.prev = left
        node.next = nil
        node.prev = nil
        if right == nil {
            tail = left
        }
        if left == nil {
            head = right
        }
    }

    private mutating func insertBetween(left: LLNode<T>?, right: LLNode<T>?, node: LLNode<T>) {
        left?.next = node
        node.prev = left
        node.next = right
        right?.prev = node
        if left == nil {
            head = node
        }
        if right == nil {
            tail = node
        }
    }

    var empty: Bool {
        return head == nil
    }

    var front: T {
        return head!.v
    }

    var back: T {
        return tail!.v
    }

    private(set) var head: LLNode<T>?
    private var tail: LLNode<T>?
}
