//
//  LinkedListTests.swift
//  FlickrClientTests
//
//  Created by Efimov, Michael on 13/04/2019.
//  Copyright © 2019 Efimov, Michael. All rights reserved.
//

import Foundation
import XCTest

@testable import FlickrClient

class LinkedListTests: XCTestCase {

    func testEmpty() {
        let list = LinkedList<Int>()
        expect(list: list, equalTo: [])
    }

    func testPushFrontOnce() {
        var list = LinkedList<Int>()
        list.pushFront(1)
        expect(list: list, equalTo: [1])
    }

    func testPushFrontTwice() {
        var list = LinkedList<Int>()
        list.pushFront(1)
        list.pushFront(2)
        expect(list: list, equalTo: [2, 1])
    }

    func testPushBackOnce() {
        var list = LinkedList<Int>()
        list.pushBack(1)
        expect(list: list, equalTo: [1])
    }

    func testPushBackTwice() {
        var list = LinkedList<Int>()
        list.pushBack(1)
        list.pushBack(2)
        expect(list: list, equalTo: [1, 2])
    }

    func testPushFrontBack() {
        var list = LinkedList<Int>()
        list.pushFront(1)
        list.pushBack(2)
        expect(list: list, equalTo: [1, 2])
    }

    func testPopFront() {
        var list = LinkedList<Int>()
        list.pushFront(1)
        list.pushFront(2)
        list.popFront()
        expect(list: list, equalTo: [1])
    }

    func testPopBack() {
        var list = LinkedList<Int>()
        list.pushFront(1)
        list.pushFront(2)
        list.popBack()
        expect(list: list, equalTo: [2])
    }

    func testPopMiddle() {
        var list = LinkedList<Int>()
        let node = LLNode<Int>(v: 3)
        list.pushFront(node: node)
        list.pushFront(1)
        list.pushBack(2)

        expect(list: list, equalTo: [1, 3, 2])

        list.popNode(node: node)
        
        expect(list: list, equalTo: [1, 2])
    }

    private func expect(list: LinkedList<Int>, equalTo array: [Int]) {
        var node = list.head
        for elem in array {
            if let nonNilNode = node {
                XCTAssertEqual(nonNilNode.v, elem)
                node = nonNilNode.next
            } else {
                XCTAssert(false)
                return
            }
        }
        XCTAssertNil(node)
    }
}
