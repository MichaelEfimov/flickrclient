//
//  TaskRunner.swift
//  FlickrClient
//
//  Created by Efimov, Michael on 13/04/2019.
//  Copyright © 2019 Efimov, Michael. All rights reserved.
//

import Foundation

protocol TaskRunner: AnyObject {
    typealias Task = () -> Void
    func postTask(_: @escaping Task)
    func postDelayedTask(after delay: TimeInterval, _: @escaping Task)
}

protocol SequencedTaskRunner: TaskRunner {
}

protocol MainThreadTaskRunner: SequencedTaskRunner {
}
