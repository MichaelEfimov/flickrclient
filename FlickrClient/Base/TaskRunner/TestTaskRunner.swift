//
//  TestTaskRunner.swift
//  FlickrClientTests
//
//  Created by Efimov, Michael on 13/04/2019.
//  Copyright © 2019 Efimov, Michael. All rights reserved.
//

import Foundation

@testable import FlickrClient

class TestTaskRunner: MainThreadTaskRunner {

    func postTask(_ task: @escaping Task) {
        self.tasks.append(task)
    }

    func postDelayedTask(after delay: TimeInterval, _ task: @escaping Task) {
        self.tasks.append(task)
    }

    func runTasksUntilIdle() {
        while self.tasks.count > 0 {
            let tasks = self.tasks
            self.tasks.removeAll()
            for task in tasks {
                task()
            }
        }
    }

    private var tasks = [Task]()
}
