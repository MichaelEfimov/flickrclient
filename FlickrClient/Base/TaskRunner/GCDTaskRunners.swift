//
//  GCDTaskRunners.swift
//  FlickrClient
//
//  Created by Efimov, Michael on 13/04/2019.
//  Copyright © 2019 Efimov, Michael. All rights reserved.
//

import Foundation

fileprivate final class GCDSerialQueueTaskRunner: SequencedTaskRunner {
    init(queue: DispatchQueue) {
        self.queue = queue
    }
    func postTask(_ task: @escaping Task) {
        self.queue.async(execute: task)
    }

    func postDelayedTask(after delay: TimeInterval, _ task: @escaping Task) {
        self.queue.asyncAfter(deadline: .now() + delay, execute: task)
    }

    private let queue: DispatchQueue
}

final class GCDQueueMainThreadTaskRunner: MainThreadTaskRunner {
    init() {
        queueRunner = GCDSerialQueueTaskRunner(queue: DispatchQueue.main)
    }

    func postTask(_ task: @escaping Task) {
        self.queueRunner.postTask(task)
    }

    func postDelayedTask(after delay: TimeInterval, _ task: @escaping Task) {
        self.queueRunner.postDelayedTask(after: delay, task)
    }

    private let queueRunner: GCDSerialQueueTaskRunner
}

final class GCDBackgroundQueueTaskRunner: SequencedTaskRunner {
    init(name: String) {
        queueRunner = GCDSerialQueueTaskRunner(queue: DispatchQueue(label: name))
    }

    func postTask(_ task: @escaping Task) {
        self.queueRunner.postTask(task)
    }

    func postDelayedTask(after delay: TimeInterval, _ task: @escaping Task) {
        self.queueRunner.postDelayedTask(after: delay, task)
    }

    private let queueRunner: GCDSerialQueueTaskRunner
}
