//
//  ImageListService.swift
//  FlickrClient
//
//  Created by Efimov, Michael on 15/04/2019.
//  Copyright © 2019 Efimov, Michael. All rights reserved.
//

import Foundation

struct ImageListServiceItem {
    let id: String
    let url: URL
}

protocol ImageListService: AnyObject {
    typealias LoadCallback = ([ImageListServiceItem]?, Int) -> Void
    func loadImages(query: String, page: Int, perPage: Int,
                    callbackTaskRunner: TaskRunner,
                    callback: @escaping LoadCallback)
}
