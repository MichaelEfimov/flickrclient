//
//  FakeImageListModel.swift
//  FlickrClientTests
//
//  Created by Efimov, Michael on 14/04/2019.
//  Copyright © 2019 Efimov, Michael. All rights reserved.
//

import Foundation

@testable import FlickrClient

class FakeImageListModel: ImageListModelProtocol {
    init(pageSize: Int = 10, taskRunner: MainThreadTaskRunner) {
        self.pageSize = pageSize
        self.taskRunner = taskRunner
    }

    private(set) var currentQuery: String = ""
    private(set) var failedLastTime: Bool = false

    var emptyNextLoad: Bool = false
    var failNextLoad: Bool = false
    let pageSize: Int

    func reload(query: String) {
        currentQuery = query
        if !emptyNextLoad {
            items = genItems(startIndex: 0, count: pageSize)
        }
        isLoading = true
        taskRunner.postTask { [weak self] in
            self?.didLoad(startIndex: 0)
        }
    }

    func loadMore() {
        if isLoading {
            return
        }
        let prevSize = items.count
        if !emptyNextLoad {
            items += genItems(startIndex: prevSize, count: pageSize)
        }
        isLoading = true
        taskRunner.postTask { [weak self] in
            self?.didLoad(startIndex: prevSize)
        }
    }

    private func didLoad(startIndex: Int) {
        isLoading = false
        if failNextLoad {
            failedLastTime = true
            delegate?.imageListModelDidFailedLoad()
        } else {
            delegate?.imageListModelDidLoad(startIndex: startIndex)
        }
    }

    private(set) var isLoading: Bool = false {
        didSet {
            delegate?.imageListModelDidUpdateLoadingState()
        }
    }

    var itemCount: Int {
        return items.count
    }

    func item(at index: Int) -> ImageListModelItem {
        return items[index]
    }

    weak var delegate: ImageListModelDelegate?

    static func itemForIndex(_ index: Int) -> ImageListModelItem {
        return ImageListModelItem(id: "\(index)", url: URL(string: "http://\(index).com")!)
    }

    private func genItems(startIndex: Int, count: Int) -> [ImageListModelItem] {
        return (startIndex ..< startIndex + count).map({
            return FakeImageListModel.itemForIndex($0)
        })
    }

    private var items = [ImageListModelItem]()
    private let taskRunner: MainThreadTaskRunner
}
