//
//  FakeImageListService.swift
//  FlickrClient
//
//  Created by Efimov, Michael on 14/04/2019.
//  Copyright © 2019 Efimov, Michael. All rights reserved.
//

import Foundation

@testable import FlickrClient

class FakeImageListService: ImageListService {

    private(set) var lastQuery: String?

    var failRequests: Bool = false

    func loadImages(query: String, page: Int, perPage: Int,
                    callbackTaskRunner: TaskRunner,
                    callback: @escaping LoadCallback) {
        lastQuery = query
        if failRequests {
            callbackTaskRunner.postTask {
                callback(nil, 0)
            }
            return
        }
        let pictures = [
            "https://www.catster.com/wp-content/uploads/2017/12/A-gray-kitten-meowing.jpg",
            "https://www.thehappycatsite.com/wp-content/uploads/2017/10/best-treats-for-kittens.jpg"
        ]
        callbackTaskRunner.postTask {
            let items = (perPage * page ..< perPage * (page + 1)).map {
                ImageListServiceItem(id: "\($0)", url: URL(string: pictures[$0 % pictures.count])!)
            }
            callback(items, 10000)
        }
    }
}
