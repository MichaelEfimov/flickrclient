//
//  ImageListModelTests.swift
//  FlickrClientTests
//
//  Created by Efimov, Michael on 14/04/2019.
//  Copyright © 2019 Efimov, Michael. All rights reserved.
//

import Foundation
import XCTest

@testable import FlickrClient

class ImageListModelTests: XCTestCase {
    override func setUp() {
        taskRunner = TestTaskRunner()
        service = FakeImageListService()
        me = ImageListModel(service: service, mainThreadTaskRunner: taskRunner, perPage: self.perPage)
        delegate = FakeImageListModelDelegate()
        me.delegate = delegate
    }

    override func tearDown() {
        me = nil
        taskRunner = nil
        service = nil
        delegate = nil
    }

    func testInit() {
        taskRunner.runTasksUntilIdle()
        XCTAssertNil(service.lastQuery)
        XCTAssertEqual(delegate.loadCounter, 0)
        XCTAssertEqual(delegate.loadStateCounter, 0)
    }

    func testReload() {
        let query = "abc"
        me.reload(query: query)
        XCTAssertEqual(me.currentQuery, query)
        XCTAssertNil(service.lastQuery)
        XCTAssertEqual(delegate.loadCounter, 0)
        taskRunner.runTasksUntilIdle()
        XCTAssertEqual(service.lastQuery, query)
        XCTAssertEqual(delegate.loadCounter, 1)
        XCTAssertEqual(delegate.lastStartIndex, 0)
    }

    func testDoubleReload() {
        let query = "abc"
        me.reload(query: query)
        XCTAssertEqual(delegate.loadCounter, 0)
        me.reload(query: query)
        XCTAssertEqual(delegate.loadCounter, 0)
        XCTAssertNil(service.lastQuery)
        taskRunner.runTasksUntilIdle()
        XCTAssertEqual(service.lastQuery, query)
        XCTAssertEqual(delegate.loadCounter, 1)
        XCTAssertEqual(delegate.lastStartIndex, 0)
    }

    func testReloadDifferentQueries() {
        let oldQuery = "abc"
        let newQuery = "def"
        me.reload(query: oldQuery)
        XCTAssertEqual(delegate.loadCounter, 0)
        me.reload(query: newQuery)
        XCTAssertEqual(delegate.loadCounter, 0)
        XCTAssertNil(service.lastQuery)
        taskRunner.runTasksUntilIdle()
        XCTAssertEqual(service.lastQuery, newQuery)
        XCTAssertEqual(delegate.loadCounter, 1)
        XCTAssertEqual(delegate.lastStartIndex, 0)
    }

    func testLoadMore() {
        let query = "abc"
        me.reload(query: query)
        taskRunner.runTasksUntilIdle()
        XCTAssertEqual(delegate.loadCounter, 1)
        XCTAssertEqual(delegate.lastStartIndex, 0)

        me.loadMore()
        taskRunner.runTasksUntilIdle()

        XCTAssertEqual(delegate.loadCounter, 2)
        XCTAssertEqual(delegate.lastStartIndex, perPage)
    }

    func testReloadWithEmpty() {
        me.reload(query: "")
        taskRunner.runTasksUntilIdle()
        XCTAssertEqual(delegate.loadCounter, 0)
    }

    func testLoadMoreWhenEmpty() {
        me.loadMore()
        taskRunner.runTasksUntilIdle()

        XCTAssertEqual(delegate.loadCounter, 0)
    }

    func testDoubleLoadMore() {
        let query = "abc"
        me.reload(query: query)
        taskRunner.runTasksUntilIdle()
        XCTAssertEqual(delegate.loadCounter, 1)
        XCTAssertEqual(delegate.lastStartIndex, 0)

        me.loadMore()
        me.loadMore()
        taskRunner.runTasksUntilIdle()

        XCTAssertEqual(delegate.loadCounter, 2)
        XCTAssertEqual(delegate.lastStartIndex, perPage)
    }

    func testReloadWhileLoadingMore() {
        let oldQuery = "abc"
        let newQuery = "def"
        me.reload(query: oldQuery)
        taskRunner.runTasksUntilIdle()

        me.loadMore()
        XCTAssertEqual(delegate.loadCounter, 1)
        me.reload(query: newQuery)
        XCTAssertEqual(delegate.loadCounter, 2)
        taskRunner.runTasksUntilIdle()
        XCTAssertEqual(me.currentQuery, newQuery)
        XCTAssertEqual(delegate.loadCounter, 3)
        XCTAssertEqual(delegate.lastStartIndex, 0)
    }

    func testFailLoad() {
        let query = "abc"
        service.failRequests = true
        me.reload(query: query)
        XCTAssertEqual(me.currentQuery, query)
        XCTAssertNil(service.lastQuery)
        taskRunner.runTasksUntilIdle()
        XCTAssertEqual(delegate.loadCounter, 0)
        XCTAssertEqual(delegate.failCounter, 1)
        XCTAssertTrue(me.failedLastTime)

        service.failRequests = false
        me.reload(query: query)
        XCTAssertEqual(delegate.loadCounter, 0)
        XCTAssertEqual(me.currentQuery, query)
        taskRunner.runTasksUntilIdle()
        XCTAssertEqual(delegate.loadCounter, 1)
        XCTAssertEqual(delegate.failCounter, 1)
    }

    private var me: ImageListModel!
    private var service: FakeImageListService!
    private var taskRunner: TestTaskRunner!
    private var delegate: FakeImageListModelDelegate!
    private let perPage = 10
}

private class FakeImageListModelDelegate: ImageListModelDelegate {
    private(set) var loadCounter = 0
    private(set) var lastStartIndex: Int?
    func imageListModelDidLoad(startIndex: Int) {
        loadCounter += 1
        lastStartIndex = startIndex
    }

    private(set) var loadStateCounter = 0
    func imageListModelDidUpdateLoadingState() {
        loadStateCounter += 1
    }

    private(set) var failCounter = 0
    func imageListModelDidFailedLoad() {
        failCounter += 1
    }
}
