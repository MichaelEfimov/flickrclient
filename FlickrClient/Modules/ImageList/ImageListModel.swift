//
//  ImageListModel.swift
//  FlickrClient
//
//  Created by Efimov, Michael on 13/04/2019.
//  Copyright © 2019 Efimov, Michael. All rights reserved.
//

import Foundation

struct ImageListModelItem {
    let id: String
    let url: URL
}

protocol ImageListModelProtocol: AnyObject {

    func reload(query: String)
    func loadMore()

    var currentQuery: String { get }

    var isLoading: Bool { get }
    var failedLastTime: Bool { get }

    var itemCount: Int { get }
    func item(at index: Int) -> ImageListModelItem

    var delegate: ImageListModelDelegate? { get set }
}

protocol ImageListModelDelegate: AnyObject {
    func imageListModelDidLoad(startIndex: Int)
    func imageListModelDidFailedLoad()
    func imageListModelDidUpdateLoadingState()
}

class ImageListModel: ImageListModelProtocol {
    init(service: ImageListService,
         mainThreadTaskRunner: MainThreadTaskRunner = GCDQueueMainThreadTaskRunner(),
         perPage: Int = 99) {
        self.service = service
        self.mainThreadTaskRunner = mainThreadTaskRunner
        self.perPage = perPage
    }

    func reload(query: String) {
        if self.currentQuery == query && self.isLoading {
            return
        }
        let wasEmpty = items.isEmpty
        items = []
        currentQuery = query
        currentPage = 0
        failedLastTime = false

        if query.isEmpty {
            self.loadID = nil
        } else {
            let loadID = LoadID()
            self.loadID = loadID
        }
        delegate?.imageListModelDidUpdateLoadingState()
        
        if !wasEmpty {
            delegate?.imageListModelDidLoad(startIndex: 0)
        }

        if !query.isEmpty {
            let loadID = self.loadID!
            mainThreadTaskRunner.postDelayedTask(after:0.3) { [weak self] in
                self?.doLoad(loadID: loadID)
            }
        }
    }

    func loadMore() {
        if isLoading {
            return
        }
        if self.currentQuery.isEmpty {
            return
        }
        if self.serverExhausted {
            return
        }
        currentPage += 1
        failedLastTime = false
        let loadID = LoadID()
        self.loadID = loadID
        delegate?.imageListModelDidUpdateLoadingState()
        doLoad(loadID: loadID)
    }

    var isLoading: Bool {
        return loadID != nil
    }

    var itemCount: Int {
        return items.count
    }

    func item(at index: Int) -> ImageListModelItem {
        return items[index]
    }

    weak var delegate: ImageListModelDelegate?

    private(set) var currentQuery: String = ""
    private(set) var failedLastTime: Bool = false

    private func doLoad(loadID: LoadID) {
        guard self.loadID === loadID else {
            return
        }
        service.loadImages(query: self.currentQuery,
                           page: self.currentPage,
                           perPage: self.perPage,
                           callbackTaskRunner: mainThreadTaskRunner) { [weak self] items, pageCount in
            self?.applyLoadedImages(loadID: loadID, items: items, pageCount: pageCount)
        }
    }

    private func applyLoadedImages(loadID: LoadID, items: [ImageListServiceItem]?, pageCount: Int) {
        guard loadID === self.loadID else {
            return
        }
        self.loadID = nil
        let prevSize = self.items.count
        guard let items = items else {
            failedLastTime = true
            delegate?.imageListModelDidFailedLoad()
            delegate?.imageListModelDidUpdateLoadingState()
            return
        }
        for item in items {
            self.items.append(ImageListModelItem(id: item.id, url: item.url))
        }
        self.serverExhausted = currentPage + 1 >= pageCount
        delegate?.imageListModelDidLoad(startIndex: prevSize)
        delegate?.imageListModelDidUpdateLoadingState()
    }

    private class LoadID {}

    private var items: [ImageListModelItem] = []
    private var currentPage: Int = 0
    private var loadID: LoadID?
    private let service: ImageListService
    private let mainThreadTaskRunner: MainThreadTaskRunner
    private let perPage: Int
    private var serverExhausted = false
}
