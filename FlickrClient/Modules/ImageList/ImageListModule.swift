//
//  ImageListModule.swift
//  FlickrClient
//
//  Created by Efimov, Michael on 13/04/2019.
//  Copyright © 2019 Efimov, Michael. All rights reserved.
//

import Foundation
import UIKit

class ImageListModule {
    init(service: ImageListService, imageManager: ImageManagerProtocol) {
        let model = ImageListModel(service: service)
        let view = ImageListViewController()
        let presenter = ImageListPresenter(view: view, model: model, imageManager: imageManager)
        self.viewController = view
        self.presenter = presenter
    }

    let viewController: UIViewController

    private let presenter: ImageListPresenter
}
