//
//  ImageListPresenterTests.swift
//  FlickrClientTests
//
//  Created by Efimov, Michael on 14/04/2019.
//  Copyright © 2019 Efimov, Michael. All rights reserved.
//

import Foundation
import UIKit
import XCTest

@testable import FlickrClient

class ImageListPresenterTests: XCTestCase {
    override func setUp() {
        taskRunner = TestTaskRunner()
        model = FakeImageListModel(taskRunner: taskRunner)
        view = FakeImageListView()
        imageManager = FakeImageManager()
        me = ImageListPresenter(view: view, model: model, imageManager: imageManager)
    }

    override func tearDown() {
        me = nil
        model = nil
        view = nil
        imageManager = nil
    }

    func testInit() {
        XCTAssertNotNil(model.delegate)
        XCTAssertNotNil(view.delegate)
        XCTAssertEqual(view.status, StringsProvider().emptyQueryString)
        XCTAssertFalse(view.showLoading)
    }

    func testReload() {
        let query = "abc"
        view.delegate?.didUpdateQuery(query: query)
        XCTAssertNil(view.status)
        XCTAssertTrue(view.showLoading)
        taskRunner.runTasksUntilIdle()
        XCTAssertNil(view.status)
        XCTAssertFalse(view.showLoading)
        XCTAssertEqual(view.items.count, model.pageSize)
        XCTAssertEqual(imageManager.cancelCalls, 1)
    }

    func testLoadMore() {
        let query = "abc"
        view.delegate?.didUpdateQuery(query: query)
        taskRunner.runTasksUntilIdle()
        XCTAssertNil(view.status)
        XCTAssertFalse(view.showLoading)
        XCTAssertEqual(view.items.count, model.pageSize)
        XCTAssertEqual(imageManager.cancelCalls, 1)

        view.delegate?.didScrollToEnd()
        XCTAssertNil(view.status)
        XCTAssertTrue(view.showLoading)
        XCTAssertEqual(view.items.count, model.pageSize)
        taskRunner.runTasksUntilIdle()
        XCTAssertNil(view.status)
        XCTAssertFalse(view.showLoading)
        XCTAssertEqual(view.items.count, model.pageSize * 2)
        XCTAssertEqual(imageManager.cancelCalls, 1)
    }

    func testFailReload() {
        model.failNextLoad = true
        let query = "abc"
        view.delegate?.didUpdateQuery(query: query)
        taskRunner.runTasksUntilIdle()
        XCTAssertEqual(view.status, StringsProvider().errorString)
        XCTAssertFalse(view.showLoading)
        XCTAssertEqual(view.items.count, 0)
    }

    func testFailLoadMore() {
        let query = "abc"
        view.delegate?.didUpdateQuery(query: query)
        taskRunner.runTasksUntilIdle()

        model.failNextLoad = true
        view.delegate?.didScrollToEnd()
        taskRunner.runTasksUntilIdle()
        XCTAssertEqual(view.status, StringsProvider().errorString)
        XCTAssertFalse(view.showLoading)
        XCTAssertEqual(view.items.count, model.pageSize)
    }

    func testEmptyReload() {
        model.emptyNextLoad = true
        let query = "abc"
        view.delegate?.didUpdateQuery(query: query)
        taskRunner.runTasksUntilIdle()
        XCTAssertEqual(view.status, StringsProvider().noResultsString(query: query))
        XCTAssertFalse(view.showLoading)
        XCTAssertEqual(view.items.count, 0)
    }

    func testEmptyLoadMore() {
        let query = "abc"
        view.delegate?.didUpdateQuery(query: query)
        taskRunner.runTasksUntilIdle()

        model.emptyNextLoad = true
        view.delegate?.didScrollToEnd()
        taskRunner.runTasksUntilIdle()
        XCTAssertNil(view.status)
        XCTAssertFalse(view.showLoading)
        XCTAssertEqual(view.items.count, model.pageSize)
    }

    func testItemsConvertSync() {
        let firstItem = FakeImageListModel.itemForIndex(0)
        let secondItem = FakeImageListModel.itemForIndex(1)
        imageManager.bind(id: firstItem.id, image: TestImages.images[0])
        imageManager.bind(id: secondItem.id, image: TestImages.images[1])
        let query = "abc"
        view.delegate?.didUpdateQuery(query: query)
        taskRunner.runTasksUntilIdle()

        XCTAssertEqual(view.items[0].image, TestImages.images[0])
        XCTAssertEqual(view.items[1].image, TestImages.images[1])
    }

    func testItemsConvertAsync() {
        let firstItem = FakeImageListModel.itemForIndex(0)
        let secondItem = FakeImageListModel.itemForIndex(1)
        imageManager.bind(id: firstItem.id, image: TestImages.images[0])
        imageManager.bind(id: secondItem.id, image: TestImages.images[1])
        imageManager.taskRunner = taskRunner
        let query = "abc"
        view.delegate?.didUpdateQuery(query: query)
        taskRunner.runTasksUntilIdle()

        XCTAssertEqual(view.items[0].image, TestImages.images[0])
        XCTAssertEqual(view.items[1].image, TestImages.images[1])
    }

    private var me: ImageListPresenter!
    private var model: FakeImageListModel!
    private var view: FakeImageListView!
    private var imageManager: FakeImageManager!
    private var taskRunner: TestTaskRunner!
}


private class FakeImageManager: ImageManagerProtocol {
    func bind(id: String, image: UIImage) {
        bindMap[id] = image
    }

    var taskRunner: MainThreadTaskRunner?

    func loadImage(id: String, url: URL, callback: @escaping LoadCalllback) {
        if let taskRunner = taskRunner {
            taskRunner.postTask { [weak self] in
                callback(self?.bindMap[id])
            }
        } else {
            callback(bindMap[id])
        }
    }

    private(set) var cancelCalls = 0
    func cancelPending() {
        cancelCalls += 1
    }

    private var bindMap = [String: UIImage]()
}

private class FakeImageListView: ImageListView {
    func reload(startIndex: Int, items: [ImageListViewItem]) {
        self.items.removeLast(self.items.count - startIndex)
        self.items += items
    }

    func update(index: Int, item: ImageListViewItem) {
        items[index] = item
    }

    weak var delegate: ImageListViewDelegate?

    var showLoading: Bool = false

    func showStatus(_ status: String?) {
        self.status = status
    }

    private(set) var status: String?

    private(set) var items = [ImageListViewItem]()
}

