//
//  ImageListPresenter.swift
//  FlickrClient
//
//  Created by Efimov, Michael on 13/04/2019.
//  Copyright © 2019 Efimov, Michael. All rights reserved.
//

import Foundation
import UIKit

struct ImageListViewItem {
    let image: UIImage?
}

protocol ImageListView: AnyObject {
    func reload(startIndex: Int, items: [ImageListViewItem])
    func update(index: Int, item: ImageListViewItem)
    var delegate: ImageListViewDelegate? { get set }
    var showLoading: Bool { get set }
    func showStatus(_ status: String?)
}

protocol ImageListViewDelegate: AnyObject {
    func didScrollToEnd()
    func didUpdateQuery(query: String)
}

protocol ImageListStringsProvider: AnyObject {
    var emptyQueryString: String { get }
    var errorString: String { get }
    func noResultsString(query: String) -> String
}

class ImageListPresenter {
    init(view: ImageListView,
         model: ImageListModelProtocol,
         imageManager: ImageManagerProtocol,
         stringsProvider: ImageListStringsProvider = StringsProvider()) {
        self.view = view
        self.model = model
        self.imageManager = imageManager
        self.stringsProvider = stringsProvider

        view.delegate = self
        model.delegate = self
        updateStatus()
    }

    private func update(startIndex: Int) {
        if startIndex == 0 {
            imageManager.cancelPending()
        }
        var items = [ImageListViewItem]()
        for index in startIndex ..< model.itemCount {
            let modelItem = model.item(at: index)
            var loadedImage: UIImage?
            var contextGone = false
            imageManager.loadImage(id: modelItem.id, url: modelItem.url) { [weak self] image in
                if contextGone {
                    self?.didUpdateImage(index: index, id: modelItem.id, image: image)
                } else {
                    loadedImage = image
                }
            }
            contextGone = true
            items.append(ImageListViewItem(image: loadedImage))
        }
        view.reload(startIndex: startIndex, items: items)
    }

    private func didUpdateImage(index: Int, id: String, image: UIImage?) {
        guard index < model.itemCount && model.item(at: index).id == id else {
            return
        }

        if let image = image {
            view.update(index: index, item: ImageListViewItem(image: image))
        }
    }

    private func updateStatus() {
        var status: String?
        if !model.isLoading {
            if model.failedLastTime {
                status = stringsProvider.errorString
            } else if model.itemCount == 0 {
                if model.currentQuery.isEmpty {
                    status = stringsProvider.emptyQueryString
                } else {
                    status = stringsProvider.noResultsString(query: model.currentQuery)
                }
            }
        }
        view.showStatus(status)
    }

    private let view: ImageListView
    private let model: ImageListModelProtocol
    private let imageManager: ImageManagerProtocol
    private let stringsProvider: ImageListStringsProvider
}

extension ImageListPresenter: ImageListViewDelegate {
    func didScrollToEnd() {
        model.loadMore()
    }

    func didUpdateQuery(query: String) {
        model.reload(query: query)
    }
}

extension ImageListPresenter: ImageListModelDelegate {
    func imageListModelDidLoad(startIndex: Int) {
        update(startIndex: startIndex)
        updateStatus()
    }

    func imageListModelDidFailedLoad() {
        updateStatus()
    }

    func imageListModelDidUpdateLoadingState() {
        view.showLoading = model.isLoading
        updateStatus()
    }
}
