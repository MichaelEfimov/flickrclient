//
//  StringsProvider.swift
//  FlickrClient
//
//  Created by Efimov, Michael on 13/04/2019.
//  Copyright © 2019 Efimov, Michael. All rights reserved.
//

import Foundation

class StringsProvider: ImageListStringsProvider {
    let emptyQueryString = "Enter something to get results"
    let errorString = "Couldn't get result right now. Please try again."

    func noResultsString(query: String) -> String {
        return "No results for \"\(query)\""
    }


}
