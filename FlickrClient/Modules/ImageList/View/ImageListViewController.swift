//
//  ImageListViewController.swift
//  FlickrClient
//
//  Created by Efimov, Michael on 13/04/2019.
//  Copyright © 2019 Efimov, Michael. All rights reserved.
//

import Foundation
import UIKit

private enum Sections: Int, CaseIterable {
    case images
    case status
    case loading
}

class ImageListViewController: UIViewController {
    init(itemsPerRow: Int = 3,
         gapWidth: CGFloat = 6) {
        self.itemsPerRow = itemsPerRow
        self.gapWidth = gapWidth
        super.init(nibName: nil, bundle: nil)
    }

    @available(*, unavailable) required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Images"
        self.view.backgroundColor = .white
        setupSearchBar()
        setupCollectionView()
        setupLayout()
    }

    weak var delegate: ImageListViewDelegate?

    var showLoading: Bool = false {
        didSet {
            if showLoading != oldValue {
                if self.isViewLoaded {
                    self.collectionView.reloadSections(IndexSet(integer: Sections.loading.rawValue))
                }
            }
        }
    }

    private func setupSearchBar() {
        let searchBar = UISearchBar(frame: .zero)
        searchBar.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(searchBar)
        searchBar.delegate = self
        self.searchBar = searchBar
    }

    private func setupCollectionView() {
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.alwaysBounceVertical = true
        self.view.addSubview(collectionView)
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.backgroundColor = .white
        collectionView.register(ImageCell.self, forCellWithReuseIdentifier: imageCellReuseID)
        collectionView.register(LoadingCell.self, forCellWithReuseIdentifier: loadingCellReuseID)
        collectionView.register(StatusCell.self, forCellWithReuseIdentifier: statusCellReuseID)
        self.collectionView = collectionView
    }

    private func setupLayout() {
        let topAnchor = self.topLayoutGuide.bottomAnchor
        NSLayoutConstraint.activate([
            searchBar.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
            searchBar.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
            searchBar.topAnchor.constraint(equalTo: topAnchor),
            searchBar.heightAnchor.constraint(equalToConstant: 44),
            collectionView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
            collectionView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
            collectionView.topAnchor.constraint(equalTo: searchBar.bottomAnchor),
            collectionView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor)
        ])
    }

    private let itemsPerRow: Int
    private let gapWidth: CGFloat
    private var collectionView: UICollectionView!
    private var searchBar: UISearchBar!
    private var items: [ImageListViewItem] = []
    private let imageCellReuseID = "ImageCell"
    private let loadingCellReuseID = "LoadingCell"
    private let statusCellReuseID = "StatusCell"
    private var status: String?
}

extension ImageListViewController: ImageListView {
    func reload(startIndex: Int, items: [ImageListViewItem]) {
        self.items.removeLast(self.items.count - startIndex)
        self.items += items
        if self.isViewLoaded {
            self.collectionView.reloadSections(IndexSet(integer: Sections.images.rawValue))
        }
    }

    func update(index: Int, item: ImageListViewItem) {
        self.items[index] = item
        if self.isViewLoaded {
            self.collectionView.reloadItems(at: [IndexPath(row: index, section: Sections.images.rawValue)])
        }
    }

    func showStatus(_ status: String?) {
        self.status = status
        if self.isViewLoaded {
            self.collectionView.reloadSections(IndexSet(integer: Sections.status.rawValue))
        }
    }
}

extension ImageListViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let section = Sections(rawValue: indexPath.section)!
        switch section {
        case .images:
            let allGapWidth = gapWidth * CGFloat(itemsPerRow + 1)
            let widthPerItem = max(0, collectionView.bounds.size.width - allGapWidth) / CGFloat(itemsPerRow)
            return CGSize(width: widthPerItem, height: widthPerItem)
        case .status:
            return CGSize(width: collectionView.bounds.size.width, height: 50)
        case .loading:
            return CGSize(width: collectionView.bounds.size.width, height: 100)
        }
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        let section = Sections(rawValue: section)!
        switch section {
        case .images:
            return UIEdgeInsets(top: gapWidth, left: gapWidth, bottom: gapWidth, right: gapWidth)
        case .loading, .status:
            return UIEdgeInsets.zero
        }
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return gapWidth
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return gapWidth
    }

    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        searchBar.resignFirstResponder()
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentSize.height - scrollView.contentOffset.y - scrollView.bounds.size.height < 50 {
            delegate?.didScrollToEnd()
        }
    }
}

extension ImageListViewController: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return Sections.allCases.count
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let section = Sections(rawValue: section)!
        switch section {
        case .images:
            return items.count
        case .status:
            return status != nil ? 1 : 0
        case .loading:
            return showLoading ? 1 : 0
        }
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let section = Sections(rawValue: indexPath.section)!
        switch section {
        case .images:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: imageCellReuseID, for: indexPath) as! ImageCell
            cell.image = items[indexPath.row].image
            return cell
        case .status:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: statusCellReuseID, for: indexPath) as! StatusCell
            cell.status = status
            return cell
        case .loading:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: loadingCellReuseID, for: indexPath) as! LoadingCell
            cell.startAnimating()
            return cell
        }
    }
}

extension ImageListViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.delegate?.didUpdateQuery(query: searchText)
    }
}
