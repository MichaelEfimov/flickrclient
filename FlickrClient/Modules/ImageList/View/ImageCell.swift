//
//  ImageCell.swift
//  FlickrClient
//
//  Created by Efimov, Michael on 13/04/2019.
//  Copyright © 2019 Efimov, Michael. All rights reserved.
//

import UIKit

class ImageCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.contentView.backgroundColor = .gray

        let imageView = UIImageView(frame: self.contentView.bounds)
        imageView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        self.contentView.addSubview(imageView)
        self.imageView = imageView
    }

    @available(*, unavailable) required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    var image: UIImage? {
        didSet {
            self.imageView.alpha = 0
            self.imageView.image = image
            UIView .animate(withDuration: 0.20) {
                self.imageView.alpha = 1
            }
        }
    }

    private var imageView: UIImageView!
}
