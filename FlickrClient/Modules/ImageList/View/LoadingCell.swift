//
//  LoadingCell.swift
//  FlickrClient
//
//  Created by Efimov, Michael on 13/04/2019.
//  Copyright © 2019 Efimov, Michael. All rights reserved.
//

import Foundation
import UIKit

class LoadingCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)

        let activityIndicator = UIActivityIndicatorView(style: .gray)
        self.contentView.addSubview(activityIndicator)
        activityIndicator.frame = self.contentView.bounds
        activityIndicator.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.activityIndicator = activityIndicator
    }

    func startAnimating() {
        activityIndicator.startAnimating()
    }

    @available(*, unavailable) required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private var activityIndicator: UIActivityIndicatorView!
}
