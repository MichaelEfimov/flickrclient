//
//  FlickrServiceModule.swift
//  FlickrClient
//
//  Created by Efimov, Michael on 14/04/2019.
//  Copyright © 2019 Efimov, Michael. All rights reserved.
//

import Foundation

class FlickrServiceModule {
    init() {
        service = FlickrService()
    }

    let service: ImageListService
}
