//
//  FlickrService.swift
//  FlickrClient
//
//  Created by Efimov, Michael on 14/04/2019.
//  Copyright © 2019 Efimov, Michael. All rights reserved.
//

import Foundation

class FlickrService: ImageListService {
    init(apiKey: String = theApiKey,
         urlLoaderFactory: URLLoaderFactoryProtocol = URLLoaderFactory(),
         parseTaskRunner: SequencedTaskRunner = GCDBackgroundQueueTaskRunner(name: "FlickrServiceParse")) {
        self.apiKey = apiKey
        self.urlLoaderFactory = urlLoaderFactory
        self.parseTaskRunner = parseTaskRunner
    }

    func loadImages(query: String,
                    page: Int,
                    perPage: Int,
                    callbackTaskRunner: TaskRunner,
                    callback: @escaping LoadCallback) {
        guard let escapedQuery = query.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed),
              let url = URL(string: "https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=\(apiKey)&format=json&nojsoncallback=1&safe_search=1&text=\(escapedQuery)&page=\(page + 1)&per_page=\(perPage)") else {
            callbackTaskRunner.postTask {
                callback(nil, 0)
            }
            return
        }

        let taskID = NSObject()
        let task = urlLoaderFactory.buildURLLoader(url: url, disableCache: false, callbackTaskRunner: parseTaskRunner) { [weak self] data in
            self?.tasks.removeValue(forKey: taskID)
            guard let data = data,
                  let jsonResponse = (try? JSONSerialization.jsonObject(with: data)) as? [String: Any],
                  let photos = jsonResponse["photos"] as? [String: Any],
                  let pageCount = photos["pages"] as? Int,
                  let photoList = photos["photo"] as? [[String: Any]] else {
                callbackTaskRunner.postTask {
                    callback(nil, 0)
                }
                return
            }
            var items = [ImageListServiceItem]()
            for photo in photoList {
                if let id = photo["id"] as? String,
                   let secret = photo["secret"] as? String,
                   let farm = photo["farm"] as? Int,
                   let server = photo["server"] as? String,
                   let url = URL(string: "https://farm\(farm).staticflickr.com/\(server)/\(id)_\(secret).jpg") {
                    items.append(ImageListServiceItem(id: id, url: url))
                }
            }
            callbackTaskRunner.postTask {
                callback(items, pageCount)
            }
        }
        parseTaskRunner.postTask { [weak self] in
            guard let self = self else {
                return
            }
            self.tasks[taskID] = task
        }
        task.resume()
    }


    private let apiKey: String
    private let urlLoaderFactory: URLLoaderFactoryProtocol
    private let parseTaskRunner: SequencedTaskRunner
    private var tasks = [NSObject: URLLoaderProtocol]()
}

private let theApiKey = "3e7cc266ae2b0e0d78e279ce8e361736"
