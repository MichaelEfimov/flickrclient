## Description

This is a client application for showing images from Flickr service.

There are two main "business" modules: "Flickr" module and "ImageList" module.

"Flickr" module is a tiny module that encapsulates flickr server specific logic

"ImageList" module is a general module to show a search bar and a list of images corresponding to the current query.
It's split into three main parts: View, Presenter and Model.

There is also a bunch of reusable "base" classes, largest of which is "ImageManager".
ImageManager is a class responsible for loading and caching images.
When the image is downloaded, it's stored in both persistent cache and inmemory cache.
Inmemory cache can be drained sometimes to help with memory issues.
If the image is in persistent cache then it's loaded, when needed, and stored in inmemory cache.
Both inmemory cache and persistent cache are limited by number of elements.

## Possible improvements

Possible improvements in terms of product:

* Gallery view could be added to show large images on tap
* Search bar logic could be improved to hide it when it's not needed
* Images could be shown in their natural proportion(or closer to it, at least).

Possible internal improvements:

* Prioritize download by scroll position. Images which are shown right now could be prioritized higher for download rather then those which are off screen.
* Caches could be tuned. Size of caches and caching logic could be tuned according to expected cache hits and memory/storage usage. Additionaly, both caches could also have a limit by total size in MB.
* Network failure handling could be improved. It could be decided if retry on failure is needed at all. If it's needed, then network reachability observing could be added and a backoff interval for retries.

Possible code improvements:

* Add more unit tests. I didn't probably cover everything. So one could take a closer look at each class and add more tests.
* Add snapshot tests. Most of the logic is easily covered with unit tests, but there is a tiny level(View) for which snapshot testing could be used.
* Search bar could be extracted into a separate module, if needed. Search bars tend to be quite complex by itself(having, for example an autocompletion logic, or complex visual logic), so extracting could help with SRP and future reuse.
* Flickr service interaction could be checked better. It looks like some images are not accessiable via the described pattern.
